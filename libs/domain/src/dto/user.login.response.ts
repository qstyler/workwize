import { UserRegisterPayload } from './user.register.payload';

export class UserLoginResponse {
  accessToken: string;
  expires: number;
  jwtPayload: UserRegisterPayload;
}
