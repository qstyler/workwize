import { Type } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';

export class AddToCartDto {
  @Type(() => Number)
  @IsNotEmpty()
  id: number;

  @Type(() => Number)
  quantity = 1;
}
