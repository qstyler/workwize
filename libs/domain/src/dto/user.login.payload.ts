import { IsEmail, IsNotEmpty } from 'class-validator';

export class UserLoginPayload {
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  password: string;
}
