import { IsBoolean, IsEmail, IsNotEmpty } from 'class-validator';
import { Type } from 'class-transformer';

export class UserRegisterPayload {
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  password: string;

  @Type(() => Boolean)
  @IsBoolean()
  isSupplier = false;
}
