import { Type } from 'class-transformer';
import { IsNumber, IsString } from 'class-validator';

export class UpdateProductDto  {
  @Type(() => String)
  @IsString()
  name?: string;

  @Type(() => Number)
  @IsNumber()
  price?: number;
}
