import { User } from './user';
import { ProductData } from './product.data';

export interface Product {
  id: number;
  supplier: User;
  productData: ProductData;
}
