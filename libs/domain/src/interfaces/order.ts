import { User } from './user';
import { OrderProduct } from './order.product';

export interface Order {
  id: number;
  user: User;
  completed: boolean;
  date?: Date;
  orderProducts: OrderProduct[];
}
