import { Product } from './product';
import { Order } from './order';

export interface OrderProduct {
  id: number;
  product: Product;
  quantity: number;
  order?: Order;
}
