export interface User {
  id: number;
  email: string;
  password: string;
  isSupplier: boolean;
}
