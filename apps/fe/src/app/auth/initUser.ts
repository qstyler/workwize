import { UserState } from '../interfaces/interfaces';

const initUser = (): UserState => {
  return (
    JSON.parse(localStorage.getItem('user-login-crud') as string) || {
      name: '',
      login: false,
    }
  );
};

export default initUser;
