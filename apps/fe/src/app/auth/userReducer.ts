import { UserAction } from '../actions/actionUserLogin';
import { UserState } from '../interfaces/interfaces';

const userReducer = (state: UserState, action: UserAction): UserState => {
  switch (action.type) {
    case 'login':
      return {
        auth: action.payload,
        login: true,
      };

    case 'logout':
      return {
        login: false,
      };

    default:
      return state;
  }
};

export default userReducer;
