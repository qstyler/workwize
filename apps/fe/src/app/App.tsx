import React from 'react';
import UserProvider from './context/user/UserProvider';
import AppRouter from './routers/AppRouter';
import CartProvider from './context/cart/CartProvider';

const App = () => {
  return (
    <div>
      <UserProvider>
        <CartProvider>
          <AppRouter />
        </CartProvider>
      </UserProvider>
    </div>
  );
};

export default App;
