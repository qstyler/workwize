import { UserLoginPayload, UserLoginResponse, UserRegisterPayload } from '@workwize/domain';

import { environment } from '../../environments/environment';

import axios from 'axios';

export type UserAction =
  | { type: 'login'; payload: UserLoginResponse }
  | { type: 'logout' };

const url = `${environment.apiUrl}/auth`;

export const doLogin = async (payload: UserLoginPayload): Promise<UserAction> => {

  const { data } = await axios.post<UserLoginResponse>(`${url}/login`, payload);

  return {
    type: 'login',
    payload: data,
  };
};

export const doRegister = async (payload: UserRegisterPayload): Promise<UserAction> => {
  const { data } = await axios.post<UserLoginResponse>(`${url}/register`, payload);

  return {
    type: 'login',
    payload: data,
  };
};

export const doLogout = (): UserAction => {
  return {
    type: 'logout',
  };
};
