import React, { useContext } from 'react';
import { Link, NavLink, useHistory } from 'react-router-dom';
import { UserContext } from '../../context/user/UserContext';
import { doLogout } from '../../actions/actionUserLogin';
import { CartContext } from '../../context/cart/CartContext';

const NavBar = () => {
  const history = useHistory();

  const { userState, userDispatch } = useContext(UserContext);
  const { orderProducts } = useContext(CartContext);

  const cart = orderProducts.reduce((a, c) => a + c.quantity, 0);

  const handleLogoutClick = () => {
    userDispatch(doLogout());
    history.replace('/login');
  };

  const user = userState.auth?.jwtPayload;

  return (
    <nav className='navbar navbar-expand-sm navbar-dark bg-dark'>
      <Link className='navbar-brand' to='/'>
        WorkWize
      </Link>
      <div className='collapse navbar-collapse' id='navbarText'>
        <ul className='navbar-nav mr-auto'>

          <li className='nav-item'>
            <NavLink exact activeClassName='active' className='nav-link' to='/'>
              Home
            </NavLink>
          </li>

          {user?.isSupplier && <li className='nav-item'>
            <NavLink
              exact
              activeClassName='active'
              className='nav-link'
              to='/products'
            >
              Products
            </NavLink>
          </li>}

          {user?.isSupplier && <li className='nav-item'>
            <NavLink
              exact
              activeClassName='active'
              className='nav-link'
              to='/myorders'
            >
              Supplier Orders
            </NavLink>
          </li>}

          <li className='nav-item'>
            <NavLink
              exact
              activeClassName='active'
              className='nav-link'
              to='/search'
            >
              Search
            </NavLink>
          </li>

          <li className='nav-item'>
            <NavLink
              exact
              activeClassName='active'
              className='nav-link'
              to='/orders'
            >
              Customer orders
            </NavLink>
          </li>

          <li className='nav-item'>
            <NavLink
              exact
              activeClassName='active'
              className='nav-link'
              to='/cart'
            >
              Cart ({cart})
            </NavLink>
          </li>

        </ul>
      </div>
      <span className='navbar-text text-info'>{user?.email}</span>
      <button
        onClick={handleLogoutClick}
        type='button'
        className='btn text-secondary'
      >
        Logout
      </button>
    </nav>
  );
};

export default NavBar;
