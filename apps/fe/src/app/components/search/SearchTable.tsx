import React, { useContext, useEffect, useState } from 'react';
import { SearchContext } from '../../context/search/SearchContext';
import SearchTableItem from './SearchTableItem';
import { CartContext } from '../../context/cart/CartContext';

const SearchTable = () => {
  const { productList, getProducts } = useContext(SearchContext);
  const { addToCart } = useContext(CartContext);

  const [supplierId, setSupplierId] = useState<number>();

  useEffect(() => {
    getProducts(supplierId);
  }, [supplierId]);

  return (
    <div>
      {supplierId && <button
        type='button'
        className='btn btn-outline-warning me-3'
        onClick={() => setSupplierId(undefined)}
      >
        Remove filter
      </button>}
      <table className='table table-dark'>
        <thead>
        <tr>
          <th>ID</th>
          <th>Product</th>
          <th>Supplier</th>
          <th>Price</th>
          <th>Actions</th>
        </tr>
        </thead>
        {productList.length === 0 ? (
          <tbody>
          <tr>
            <td colSpan={3}>There are no products matching your criteria</td>
          </tr>
          </tbody>
        ) : (
          <tbody>
          {productList.map((product, index) => (
            <SearchTableItem
              key={product.id}
              product={product}
              filter={setSupplierId}
              addToCart={addToCart}
            />
          ))}
          </tbody>
        )}
      </table>
    </div>
  );
};

export default SearchTable;
