import React from 'react';
import SearchProvider from '../../context/search/SearchProvider';
import SearchTable from './SearchTable';

const Search = () => {
  return (
    <div>
      <SearchProvider>
        <h1>Your products:</h1>
        <div>
          <SearchTable />
        </div>
      </SearchProvider>
    </div>
  );
};

export default Search;
