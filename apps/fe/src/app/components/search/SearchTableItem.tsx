import React from 'react';
import { AddToCartDto, Product } from '@workwize/domain';

interface TableItemProps {
  product: Product;
  filter: (id: number) => void | Promise<void>;
  addToCart: (payload: AddToCartDto) => void | Promise<void>;
}

const SearchTableItem = ({ product, filter, addToCart }: TableItemProps) => {
  return (
    <tr>
      <td>{product.id}</td>
      <td>{product.productData.name}</td>
      <td>
        <a onClick={() => filter(product.supplier.id)}>
          {product.supplier.email}
        </a>
      </td>
      <td>{product.productData.price}</td>
      <td>
        <button
          type='button'
          className='btn btn-outline-warning me-3'
          onClick={() => addToCart({ id: product.id, quantity: 1 })}
        >
          Add to cart
        </button>
      </td>
    </tr>

  );
};

export default SearchTableItem;
