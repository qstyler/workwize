import React from 'react';
import { Order } from '@workwize/domain';
import { MyOrdersProps } from './Orders';

interface TableItemProps {
  index: number;
  order: Order;
}

const OrderTableItem = ({ index, order, my }: TableItemProps& MyOrdersProps) => {

  const orderTotal = order.orderProducts
    .reduce((acc, orderProduct) =>
        acc + orderProduct.quantity * orderProduct.product.productData.price
      , 0);

  return (
    <tr>
      <td>{order.id}</td>
      {my && <td>{order.user.email}</td>}
      <td>

        <table width={'100%'}>
          {order.orderProducts.map(orderProduct => (
            <tr key={orderProduct.product.id}>
              <td>{orderProduct.product.productData.name}</td>
              <td>{orderProduct.quantity}x{orderProduct.product.productData.price}</td>
            </tr>
          ))}
        </table>


      </td>
      <td>{orderTotal}</td>

    </tr>
  );
};

export default OrderTableItem;
