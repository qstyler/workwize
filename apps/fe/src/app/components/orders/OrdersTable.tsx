import React, { useContext, useEffect } from 'react';
import { MyOrdersContext } from '../../context/myorders/MyOrdersContext';
import OrderTableItem from './OrderTableItem';
import { MyOrdersProps } from './Orders';

const OrdersTable = ({ my }: MyOrdersProps) => {
  const { myOrdersList } = useContext(MyOrdersContext);
  useEffect(() => {
  }, [myOrdersList]);

  return (
    <div>
      <table className='table table-dark'>
        <thead>
        <tr>
          <th>ID</th>
          {my && <th>Buyer</th> }
          <th>Products</th>
          <th>Total</th>
        </tr>
        </thead>

        {myOrdersList.length === 0 ? (
          <tbody>
          <tr>
            <td colSpan={3}>You have no orders yet</td>
          </tr>
          </tbody>
        ) : (
          <tbody>
          {myOrdersList.map((order, index) => (
            <OrderTableItem
              key={order.id}
              index={index}
              order={order}
              my={my}
            />
          ))}
          </tbody>
        )}

      </table>
    </div>
  );
};

export default OrdersTable;
