import React from 'react';
import MyOrdersProvider from '../../context/myorders/MyOrdersProvider';
import OrdersTable from './OrdersTable';

export interface MyOrdersProps {
  my: boolean;
}

const Orders = ({ my }: MyOrdersProps) => {
  return (
    <div>
      <MyOrdersProvider my={my}>
        <h1>My orders:</h1>
        <div>
          <OrdersTable my={my}/>
        </div>
      </MyOrdersProvider>
    </div>
  );
};

export default Orders;
