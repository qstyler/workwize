import React, { useContext, useEffect, useState } from 'react';
import ProductsTableItem from './ProductsTableItem';
import ProductFormEditMode from './ProductFormEditMode';
import { ProductContext } from '../../context/product/ProductContext';
import { Product } from '@workwize/domain';

export interface EditMode {
  state: boolean;
  product: Product;
}

const ProductsTable = () => {
  const { productsList, deleteProduct } = useContext(ProductContext);
  useEffect(() => {
  }, [productsList]);

  const [editMode, setEditMode] = useState<EditMode>({
    state: false,
    product: null!,
  });

  return (
    <div>
      <table className='table table-dark'>
        <thead>
        <tr>
          <th>N°</th>
          <th>ID</th>
          <th>Product</th>
          <th>Price</th>
          <th>Actions</th>
        </tr>
        </thead>
        {productsList.length === 0 ? (
          <tbody>
          <tr>
            <td colSpan={3}>You have no products yet</td>
          </tr>
          </tbody>
        ) : (
          <tbody>
          {productsList.map((product, index) => (
            <ProductsTableItem
              key={product.id}
              index={index}
              product={product}
              deleteProduct={deleteProduct}
              setEditMode={setEditMode}
            />
          ))}
          </tbody>
        )}
      </table>
      {editMode.state && (
        <ProductFormEditMode editMode={editMode} setEditMode={setEditMode} />
      )}
    </div>
  );
};

export default ProductsTable;
