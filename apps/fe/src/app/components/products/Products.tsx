import React from 'react';
import ProductsTable from './ProductsTable';
import ProductProvider from '../../context/product/ProductProvider';
import ProductForm from './ProductForm';

const Products = () => {
  return (
    <div>
      <ProductProvider>
        <h1>Your products:</h1>
        <div>
          <ProductForm />
          <ProductsTable />
        </div>
      </ProductProvider>
    </div>
  );
};

export default Products;
