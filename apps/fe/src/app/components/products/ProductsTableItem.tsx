import React from 'react';
import { EditMode } from './ProductsTable';
import { Product } from '@workwize/domain';

interface TableItemProps {
  index: number;
  product: Product;
  deleteProduct: (id: number) => void | Promise<void>;
  setEditMode: (editMode: EditMode) => void | Promise<void>;
}

const ProductsTableItem = ({
                             index,
                             product,
                             deleteProduct,
                             setEditMode,
                           }: TableItemProps) => {
  const handleEditMode = (product: Product) => {
    setEditMode({
      state: true,
      product: product,
    });
  };

  return (
    <tr>
      <td>{index + 1}</td>
      <td>{product.id}</td>
      <td>{product.productData.name}</td>
      <td>{product.productData.price}</td>
      <td>
        <button
          onClick={() => handleEditMode(product)}
          type='button'
          className='btn btn-outline-warning me-3'
        >
          Edit
        </button>
        <button
          type='button'
          className='btn btn-outline-danger'
          onClick={() => deleteProduct(product.id)}
        >
          Delete
        </button>
      </td>
    </tr>
  );
};

export default ProductsTableItem;
