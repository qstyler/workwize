import React, { useContext } from 'react';
import { useForm } from 'react-hook-form';
import { ProductContext } from '../../context/product/ProductContext';
import { CreateProductDto, ProductData } from '@workwize/domain';
import { classValidatorResolver } from '@hookform/resolvers/class-validator';

const resolver = classValidatorResolver(CreateProductDto);

const ProductForm = () => {

  const { addProduct } = useContext(ProductContext);

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<ProductData>({ resolver });

  const add = async (payload: ProductData) => {
    addProduct(payload);
    reset();
  };

  return (
    <div>
      <form onSubmit={handleSubmit(add)}>
        <div className='mb-3'>
          <label className='form-label'>Product name</label>
          <input
            {...register('name')}
            className='form-control mb-3'
          />
          {errors.name && <div>{errors.name.message}</div>}
        </div>

        <div className='mb-3'>
          <label className='form-label'>Product price</label>
          <input
            {...register('price')}
            className='form-control mb-3'
          />
          {errors.price && <div>{errors.price.message}</div>}
        </div>

        <button
          type='submit'
          className='btn btn-outline-warning mb-3 me-3'
        >
          Save
        </button>

      </form>
    </div>
  );
};

export default ProductForm;
