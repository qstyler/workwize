import React, { useContext } from 'react';
import { ProductContext } from '../../context/product/ProductContext';
import { EditMode } from './ProductsTable';
import { useForm } from 'react-hook-form';
import { CreateProductDto, ProductData } from '@workwize/domain';
import { classValidatorResolver } from '@hookform/resolvers/class-validator';

interface FormEditModeProps {
  setEditMode: (editMode: EditMode) => void;
  editMode: EditMode;
}

const resolver = classValidatorResolver(CreateProductDto);

const ProductFormEditMode = ({ editMode, setEditMode }: FormEditModeProps) => {
  const { editProduct } = useContext(ProductContext);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<ProductData>({
    defaultValues: editMode.product.productData,
    resolver,
  });

  const edit = (payload: ProductData) => {
    editProduct(editMode.product.id, payload);
    cancel();
  };

  const cancel = () => {
    setEditMode({
      state: false,
      product: null!,
    });
  };

  return (
    <div>
      <form onSubmit={handleSubmit(edit)}>
        <div>
          <label className='form-label'>Product name</label>
          <input
            {...register('name')}
            className='form-control mb-3'
          />
          {errors.name && <div>{errors.name.message}</div>}
        </div>

        <div className='mb-3'>
          <label className='form-label'>Product price</label>
          <input
            {...register('price')}
            className='form-control mb-3'
          />
          {errors.price && <div>{errors.price.message}</div>}
        </div>

        <div className='mb-3'>
          <button
            type='submit'
            className='btn btn-outline-warning mb-3 me-3'
          >
            Save
          </button>

          <button
            type='button'
            className='btn btn-outline-danger mb-3'
            onClick={cancel}
          >
            Cancel
          </button>
        </div>
      </form>
    </div>
  );
};

export default ProductFormEditMode;
