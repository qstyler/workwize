import React from 'react';
import { useHistory } from 'react-router-dom';

const Home = () => {
  const history = useHistory();
  const handleGoClick = () => {
    history.push('/search');
  };
  return (
    <div>
      <h2>Home Screen</h2>
      <hr />
      <p>
        Shop for products.
      </p>
      <button
        type='button'
        className='btn btn-outline-primary'
        onClick={handleGoClick}
      >
        Go
      </button>
    </div>
  );
};

export default Home;
