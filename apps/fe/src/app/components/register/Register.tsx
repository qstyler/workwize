import React, { useContext } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { doRegister } from '../../actions/actionUserLogin';
import { UserContext } from '../../context/user/UserContext';
import { useForm } from 'react-hook-form';
import { UserRegisterPayload } from '@workwize/domain';
import { classValidatorResolver } from '@hookform/resolvers/class-validator';

const resolver = classValidatorResolver(UserRegisterPayload);

const Register = () => {
  const { userDispatch } = useContext(UserContext);

  const history = useHistory();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<UserRegisterPayload>({ resolver });

  const handleRegisterSubmit = async (payload: UserRegisterPayload) => {
    userDispatch(await doRegister(payload));
    history.replace('/');
  };

  return (
    <div className='mx-5 my-5'>
      <h2>Register</h2>
      <hr />
      <form onSubmit={handleSubmit(handleRegisterSubmit)}>
        <div className='mb-3 has-validation'>
          <label>Email</label>
          <input
            {...register('email')}
            className='form-control mb-3'
          />
          {errors.email && <div>{errors.email.message}</div>}
        </div>
        <div className='mb-3'>
          <label>Password</label>
          <input
            className='form-control mb-3'
            {...register('password')}
            type='password'
          />
          {errors.password && <div>{errors.password.message}</div>}
        </div>
        <div className='mb-3 form-check'>
          <input
            className='form-check-input'
            {...register('isSupplier')}
            type='checkbox'
            id='isSupplier'
          />
          <label className='form-check-label' htmlFor={'isSupplier'}>Supplier?</label>
        </div>
        <div className='mb-3'>
          <button type='submit' className='btn btn-outline-success'>
            Go
          </button>
        </div>
      </form>
      <hr />
      <Link to='/login'>
        Back to login screen
      </Link>
    </div>
  );
};

export default Register;
