import React from 'react';
import CartTable from './CartTable';

const Cart = () => {
  return (
    <div>
      <h1>Your products:</h1>
      <div>
        <CartTable />
      </div>
    </div>
  );
};

export default Cart;
