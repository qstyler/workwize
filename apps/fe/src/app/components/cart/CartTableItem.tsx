import React from 'react';
import { AddToCartDto, OrderProduct } from '@workwize/domain';

interface TableItemProps {
  orderProduct: OrderProduct;
  addToCart: (payload: AddToCartDto) => void | Promise<void>;
}

const CartTableItem = ({ orderProduct, addToCart }: TableItemProps) => {
  return (
    <tr>
      <td>{orderProduct.product.id}</td>
      <td>{orderProduct.product.productData.name}</td>
      <td>{orderProduct.product.supplier.email}</td>
      <td>{orderProduct.product.productData.price}</td>
      <td>{orderProduct.quantity}</td>
      <td>
        <button
          type='button'
          className='btn btn-outline-danger me-3'
          onClick={() => addToCart({
            id: orderProduct.product.id,
            quantity: -orderProduct.quantity,
          })}
        >
          Remove from cart
        </button>
      </td>
    </tr>

  );
};

export default CartTableItem;
