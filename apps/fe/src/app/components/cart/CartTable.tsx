import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import CartTableItem from './CartTableItem';
import { CartContext } from '../../context/cart/CartContext';

const CartTable = () => {
  const { orderProducts, addToCart, checkout } = useContext(CartContext);

  const history = useHistory();

  const doCheckout = async () => {
    await checkout();
    history.replace('/orders');
  }

  return (
    <div>
      <table className='table table-dark'>
        <thead>
        <tr>
          <th>ID</th>
          <th>Product</th>
          <th>Supplier</th>
          <th>Price</th>
          <th>Quantity</th>
          <th>Actions</th>
        </tr>
        </thead>
        {orderProducts.length === 0 ? (
          <tbody>
          <tr>
            <td colSpan={3}>There are no products in your cart</td>
          </tr>
          </tbody>
        ) : (
          <tbody>
          {orderProducts.map((orderProduct, index) => (
            <CartTableItem
              key={orderProduct.product.id}
              orderProduct={orderProduct}
              addToCart={addToCart}
            />
          ))}
          </tbody>
        )}
      </table>

      <button
        type='button'
        className='btn btn-outline-success me-3'
        onClick={doCheckout}
      >
        Checkout
      </button>

    </div>
  );
};

export default CartTable;
