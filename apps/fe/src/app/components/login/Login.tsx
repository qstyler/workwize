import React, { useContext } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { doLogin } from '../../actions/actionUserLogin';
import { UserContext } from '../../context/user/UserContext';
import { useForm } from 'react-hook-form';
import { UserLoginPayload } from '@workwize/domain';
import { classValidatorResolver } from '@hookform/resolvers/class-validator';
import { AxiosError, AxiosResponse } from 'axios';

const resolver = classValidatorResolver(UserLoginPayload);

const Login = () => {
  const { userDispatch } = useContext(UserContext);

  const history = useHistory();

  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm<UserLoginPayload>({ resolver });

  const handleLoginSubmit = async (payload: UserLoginPayload) => {
    let action;

    try {
      action = await doLogin(payload);

      userDispatch(action);
      history.replace('/');
    } catch (e) {
      setError('password',{
        type: 'manual',
        message: (e as AxiosError).response?.data.message
      });
    }
  };

  return (
    <div className='mx-5 my-5'>
      <h2>Login</h2>
      <hr />
      <form onSubmit={handleSubmit(handleLoginSubmit)}>

        <div className='mb-3'>
          <label>Email</label>
          <input
            {...register('email')}
            className='form-control mb-3'
          />
          {errors.email && <div >{errors.email.message}</div>}

        </div>
        <div className='mb-3'>
          <label>Password</label>
          <input
            className='form-control mb-3'
            {...register('password')}
            type='password'
          />
          {errors.password && <div >{errors.password.message}</div>}

        </div>
        <div className='mb-3'>
          <button type='submit' className='btn btn-outline-success'>
            Go
          </button>
        </div>
      </form>
      <hr />
      <Link to='/register'>
        Not registered yet?
      </Link>
    </div>
  );
};

export default Login;
