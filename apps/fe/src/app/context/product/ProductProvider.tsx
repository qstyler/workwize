import React, { useContext, useEffect, useState } from 'react';
import { ProductContext } from './ProductContext';
import { UserContext } from '../user/UserContext';
import axios from 'axios';
import { Product, ProductData } from '@workwize/domain';
import { environment } from '../../../environments/environment';

interface ProductsProviderProps {
  children: JSX.Element | JSX.Element[];
}


const url = `${environment.apiUrl}/products`;

const ProductProvider = ({ children }: ProductsProviderProps) => {
  const { userState } = useContext(UserContext);
  const options = {
    headers: {
      Authorization: `Bearer ${userState.auth?.accessToken}`,
    },
  };

  const [productsList, setProductsList] = useState<Product[]>([]);

  //METHOD GET
  const getProducts = async () => {
    try {
      const { data } = await axios.get(`${url}`, options);
      setProductsList(data);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    getProducts();
  }, []);

  //METHOD DELETE
  const deleteProduct = async (id: number) => {
    try {
      await axios.delete(`${url}/${id}`, options);
      getProducts();
    } catch (err) {
      console.log(err);
    }
  };

  //METHOD PUT
  const editProduct = async (id: number, body: ProductData) => {

    try {
      await axios.put(`${url}/${id}`, body, options);
      getProducts();
    } catch (err) {
      console.log(err);
    }
  };

  //METHOD POST
  const addProduct = async (body: ProductData) => {

    try {
      await axios.post(`${url}`, body, options);
      getProducts();
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <ProductContext.Provider
      value={{
        productsList,
        getProducts,
        editProduct,
        deleteProduct,
        addProduct,
      }}
    >
      {children}
    </ProductContext.Provider>
  );
};

export default ProductProvider;
