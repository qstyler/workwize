import { createContext } from 'react';
import { Product, ProductData } from '@workwize/domain';

interface ProductContextProps {
  productsList: Product[];
  getProducts: () => void | Promise<void>;
  deleteProduct: (id: number) => void | Promise<void>;
  editProduct: (id: number, body: ProductData) => void | Promise<void>;
  addProduct: (body: ProductData) => void | Promise<void>;
}

export const ProductContext = createContext<ProductContextProps>(
  {} as ProductContextProps,
);
