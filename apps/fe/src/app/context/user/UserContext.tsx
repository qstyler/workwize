import { createContext } from 'react';
import { UserState } from '../../interfaces/interfaces';
import { UserAction } from '../../actions/actionUserLogin';

interface UserContextProps {
  userState: UserState;
  userDispatch: (action: UserAction) => void;
}

export const UserContext = createContext<UserContextProps>(
  {} as UserContextProps,
);
