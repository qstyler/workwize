import React, { useContext, useEffect, useState } from 'react';
import { UserContext } from '../user/UserContext';
import axios from 'axios';
import { SearchContext } from './SearchContext';
import { Product } from '@workwize/domain';
import { environment } from '../../../environments/environment';

interface SearchProviderProps {
  children: JSX.Element | JSX.Element[];
}


const url = `${environment.apiUrl}/search`;

const SearchProvider = ({ children }: SearchProviderProps) => {
  const { userState } = useContext(UserContext);
  const options = {
    headers: {
      Authorization: `Bearer ${userState.auth?.accessToken}`,
    },
  };

  const [productList, setMyOrdersList] = useState<Product[]>([]);

  //METHOD GET
  const getProducts = async (id?: number) => {
    try {
      const { data } = await axios.get(`${url}`, {
        params: {
          ...(id ? { supplierId: id } : {}),
        },
        ...options,
      });
      setMyOrdersList(data);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    getProducts();
  }, []);

  return (
    <SearchContext.Provider
      value={{
        productList,
        getProducts,
      }}
    >
      {children}
    </SearchContext.Provider>
  );
};

export default SearchProvider;
