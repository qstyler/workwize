import { createContext } from 'react';
import { Product } from '@workwize/domain';

interface SearchContextProps {
  productList: Product[];
  getProducts: (id?: number) => void | Promise<void>;
}

export const SearchContext = createContext<SearchContextProps>(
  {} as SearchContextProps,
);
