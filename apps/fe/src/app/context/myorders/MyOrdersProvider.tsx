import React, { useContext, useEffect, useState } from 'react';
import { UserContext } from '../user/UserContext';
import axios from 'axios';
import { MyOrdersContext } from './MyOrdersContext';
import { Order } from '@workwize/domain';
import { environment } from '../../../environments/environment';

interface MyOrdersProviderProps {
  children: JSX.Element | JSX.Element[];
  my: boolean;
}

const MyOrdersProvider = ({ children, my }: MyOrdersProviderProps) => {
  const url = `${environment.apiUrl}/${my ? 'my' : ''}orders`;

  const { userState } = useContext(UserContext);
  const options = {
    headers: {
      Authorization: `Bearer ${userState.auth?.accessToken}`,
    },
  };

  const [myOrdersList, setMyOrdersList] = useState<Order[]>([]);

  //METHOD GET
  const getMyOrders = async () => {
    try {
      const { data } = await axios.get(`${url}`, options);
      setMyOrdersList(data);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    getMyOrders();
  }, []);

  return (
    <MyOrdersContext.Provider
      value={{
        myOrdersList,
        getMyOrders,
      }}
    >
      {children}
    </MyOrdersContext.Provider>
  );
};

export default MyOrdersProvider;
