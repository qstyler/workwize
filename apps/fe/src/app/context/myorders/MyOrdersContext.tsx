import { createContext } from 'react';
import { Order } from '@workwize/domain';

interface MyOrdersContextProps {
  myOrdersList: Order[];
  getMyOrders: () => void | Promise<void>;
}

export const MyOrdersContext = createContext<MyOrdersContextProps>(
  {} as MyOrdersContextProps,
);
