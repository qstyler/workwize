import { createContext } from 'react';
import { AddToCartDto, OrderProduct } from '@workwize/domain';

interface CartContextProps {
  orderProducts: OrderProduct[];
  getCart: () => void | Promise<void>;
  addToCart: (payload: AddToCartDto) => void | Promise<void>;
  checkout: () => void | Promise<void>;
}

export const CartContext = createContext<CartContextProps>(
  {} as CartContextProps,
);
