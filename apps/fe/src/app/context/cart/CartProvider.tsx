import React, { useContext, useEffect, useState } from 'react';
import { UserContext } from '../user/UserContext';
import axios from 'axios';
import { CartContext } from './CartContext';
import { AddToCartDto, OrderProduct } from '@workwize/domain';
import { environment } from '../../../environments/environment';

interface CartProviderProps {
  children: JSX.Element | JSX.Element[];
}

const url = `${environment.apiUrl}/cart`;

const CartProvider = ({ children }: CartProviderProps) => {
  const { userState } = useContext(UserContext);
  const options = {
    headers: {
      Authorization: `Bearer ${userState.auth?.accessToken}`,
    },
  };

  const [orderProducts, setCartList] = useState<OrderProduct[]>([]);

  //METHOD GET
  const getCart = async () => {
    try {
      const { data } = await axios.get(`${url}`, options);
      setCartList(data);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    getCart();
  }, []);

  const addToCart = async (payload: AddToCartDto) => {
    try {
      await axios.post(`${url}/add`, payload, options);
      getCart();
    } catch (err) {
      console.log(err);
    }
  };

  const checkout = async () => {
    try {
      await axios.post(`${url}/checkout`, {}, options);
      getCart();
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <CartContext.Provider
      value={{
        orderProducts,
        getCart,
        addToCart,
        checkout,
      }}
    >
      {children}
    </CartContext.Provider>
  );
};

export default CartProvider;
