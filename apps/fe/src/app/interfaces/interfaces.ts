import { UserLoginResponse } from '@workwize/domain';

export interface UserState {
  auth?: UserLoginResponse;
  login: boolean;
}
