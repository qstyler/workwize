import { BrowserRouter as Router, Switch } from 'react-router-dom';
import Login from '../components/login/Login';
import DashboardRouter from './DashboardRouter';
import PrivateRoute from './PrivateRoute';
import { useContext } from 'react';
import { UserContext } from '../context/user/UserContext';
import PublicRoute from './PublicRoute';
import Register from '../components/register/Register';

const AppRouter = () => {
  const { userState } = useContext(UserContext);
  return (
    <Router>
      <Switch>
        <PublicRoute
          exact
          path='/login'
          component={Login}
          allowed={userState.login}
        />
        <PublicRoute
          exact
          path='/register'
          component={Register}
          allowed={userState.login}
        />
        <PrivateRoute
          path='/'
          component={DashboardRouter}
          allowed={userState.login}
        />
      </Switch>
    </Router>
  );
};

export default AppRouter;
