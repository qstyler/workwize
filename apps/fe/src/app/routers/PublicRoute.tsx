import { Redirect, Route } from 'react-router-dom';

interface PublicRouteProps {
  allowed: boolean;
  component: () => JSX.Element;
  exact?: boolean;
  path?: string;
}

const PublicRoute = ({ allowed, component: Component, ...rest }: PublicRouteProps) => {
  return (
    <Route
      {...rest}
      component={(props: any) =>
        !allowed ? <Component {...props} /> : <Redirect to='/' />
      }
    />
  );
};

export default PublicRoute;
