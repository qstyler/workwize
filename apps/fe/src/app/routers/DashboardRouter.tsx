import React, { useContext } from 'react';

import { Redirect, Route, Switch } from 'react-router-dom';

import Home from '../components/home/Home';
import Products from '../components/products/Products';
import NavBar from '../components/ui/NavBar';
import Orders from '../components/orders/Orders';
import Search from '../components/search/Search';
import Cart from '../components/cart/Cart';
import PrivateRoute from './PrivateRoute';
import { UserContext } from '../context/user/UserContext';

const DashboardRouter = () => {
  const { userState } = useContext(UserContext);
  const user = userState.auth?.jwtPayload;

  return (
    <>
      <NavBar />
      <div className='container'>
        <Switch>
          <Route exact path='/' component={Home} />
          <PrivateRoute allowed={!!user?.isSupplier} exact path='/products' component={Products} />
          <PrivateRoute
            allowed={!!user?.isSupplier}
            exact path='/myorders'
            render={() => (
              <Orders my={true} />
            )}
          />
          <Route exact path='/orders'
                 render={() => (
                   <Orders my={false} />
                 )}
          />
          <Route exact path='/search' component={Search} />
          <Route exact path='/orders' component={Search} />
          <Route exact path='/cart' component={Cart} />

          <Redirect to='/' />
        </Switch>
      </div>
    </>
  );
};

export default DashboardRouter;
