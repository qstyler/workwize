import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { RouteComponentProps } from 'react-router';

interface PrivateRouteProps {
  allowed: boolean;
  component?: () => JSX.Element;
  render?: ((props: RouteComponentProps) => React.ReactNode);
  path?: string;
  exact?: boolean;
}

const PrivateRoute =
  ({ component: Component, render: Render, allowed, ...rest }: PrivateRouteProps) => {

    const redirect = <Redirect to='/login' />;

    const component = Component
      ? (props: any) =>
        allowed ? <Component {...props} /> : redirect
      : undefined;

    const render = Render
      ? (allowed ? Render : () => redirect)
      : undefined;

    return (
      <Route
        {...rest}
        component={component}
        render={render}
      />
    );
  };

export default PrivateRoute;
