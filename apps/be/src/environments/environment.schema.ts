import { TypeOrmModuleOptions } from '@nestjs/typeorm/dist/interfaces/typeorm-options.interface';

export interface EnvironmentSchema {
  production: boolean;
  database: TypeOrmModuleOptions;
}
