import { EnvironmentSchema } from './environment.schema';
// dirty hack
// to have it imported after production build
import * as mysql from 'mysql';
import { SecretManagerServiceClient } from '@google-cloud/secret-manager';

export const environment= async (): Promise<EnvironmentSchema> => ({
  production: false,
  database: {
    type: 'better-sqlite3',
    database: 'database.db',
    synchronize: true,
    logging: true,
  }
});
