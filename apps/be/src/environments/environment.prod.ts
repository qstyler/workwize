import { EnvironmentSchema } from './environment.schema';
import { SecretManagerServiceClient } from '@google-cloud/secret-manager';
import { TypeOrmModuleOptions } from '@nestjs/typeorm/dist/interfaces/typeorm-options.interface';


const secretManagerServiceClient = new SecretManagerServiceClient();

export const environment = async (): Promise<EnvironmentSchema> => {

  const [version] = await secretManagerServiceClient.accessSecretVersion({
    name: 'projects/637267480352/secrets/database/versions/latest'
  });

  const payload = version.payload.data.toString();

  const database = JSON.parse(payload) as TypeOrmModuleOptions;

  return {
    production: true,
    database,
  };
};
