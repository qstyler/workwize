import { Module } from '@nestjs/common';
import { CartController } from './cart.controller';
import { CartService } from './cart.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderProductSchema, OrderSchema } from '../../schemas';

@Module({
  imports: [TypeOrmModule.forFeature([OrderProductSchema, OrderSchema])],
  controllers: [CartController],
  providers: [
    CartService
  ]
})
export class CartModule {
}
