import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AddToCartDto, User } from '@workwize/domain';
import { BadRequestException } from '@nestjs/common';
import { OrderProductSchema, OrderSchema } from '../../schemas';

export class CartService {
  constructor(
    @InjectRepository(OrderProductSchema) private orderProductRepo: Repository<OrderProductSchema>,
    @InjectRepository(OrderSchema) private orderRepo: Repository<OrderSchema>,
  ) {
  }

  async getCart(authUser: User) {
    return this.orderProductRepo.find({
      where: {
        order: {
          completed: false,
          user: { id: authUser.id },
        },
      },
      relations: ['order', 'product', 'product.supplier'],
    });
  }

  async addToCart(addToCartDto: AddToCartDto, authUser: User) {
    const currentOrderId = await this.getCurrentOrderId(authUser);

    const currentOrderProduct = await this.orderProductRepo.findOne({
      where: {
        order: { id: currentOrderId },
        product: { id: addToCartDto.id },
      },
    });

    if (!currentOrderProduct) {
      return this.orderProductRepo.save({
        ...addToCartDto,
        order: { id: currentOrderId },
        product: { id: addToCartDto.id },
      });
    } else {
      const newQuantity = currentOrderProduct.quantity + addToCartDto.quantity;

      console.log();
      console.log();
      console.log(newQuantity);
      console.log();
      console.log();

      if (newQuantity === 0) {
        await this.orderProductRepo.delete(currentOrderProduct.id);
      } else {
        await this.orderProductRepo.update(currentOrderProduct.id, {
          quantity: newQuantity,
        });
      }

      return this.orderProductRepo.findOne(currentOrderProduct.id, {
        relations: ['order', 'product']
      });
    }

  }

  async getCurrentOrderId(authUser: User) {
    return (await this.orderRepo.findOne({ completed: false, user: { id: authUser.id } })
      || await this.orderRepo.save({ completed: false, user: { id: authUser.id } })).id;

  }

  async checkout(authUser: User) {
    if ((await this.getCart(authUser)).length === 0) {
      throw new BadRequestException('Cannot checkout an empty cart');
    }

    const currentOrderId = await this.getCurrentOrderId(authUser);

    await this.orderRepo.update(currentOrderId, {
      completed: true,
      date: new Date(),
    });

    return this.orderRepo.findOne(currentOrderId, {
      relations: ['orderProducts', 'orderProducts.product']
    })

  }
}
