import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-guard';
import { AuthUser } from '../auth/auth.user';
import { CartService } from './cart.service';
import { AddToCartDto, User } from '@workwize/domain';

@Controller('cart')
export class CartController {

  constructor(
    private cartService: CartService,
  ) {
  }

  @UseGuards(JwtAuthGuard)
  @Get('')
  getCart(
    @AuthUser() authUser: User,
  ) {
    return this.cartService.getCart(authUser);
  }

  @UseGuards(JwtAuthGuard)
  @Post('add')
  addToCart(
    @Body() addToCartDto: AddToCartDto,
    @AuthUser() authUser: User,
  ) {
    return this.cartService.addToCart(addToCartDto, authUser);
  }

  @UseGuards(JwtAuthGuard)
  @Post('checkout')
  checkout(
    @AuthUser() authUser: User,
  ) {
    return this.cartService.checkout(authUser);
  }

}
