import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateProductDto, UpdateProductDto, User } from '@workwize/domain';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ProductSchema } from '../../schemas';

@Injectable()
export class ProductsService {

  constructor(
    @InjectRepository(ProductSchema) private productRepo: Repository<ProductSchema>,
  ) {}

  async createProduct(productData: CreateProductDto, authUser: User) {
    return this.productRepo.save({
      productData,
      supplier: {
        id: authUser.id,
      }
    })
  }

  async findProducts(authUser: User) {
    return this.productRepo.find({
      supplier: {
        id: authUser.id,
      }
    });
  }

  async updateProduct(productId: number, product: UpdateProductDto, authUser: User) {
    const criteria = ProductsService.getCriteria(productId, authUser);

    const updateResults = await this.productRepo.update(criteria, {
      productData: product,
    });

    if (!updateResults.affected) {
      // need to create a proper ownership-checker
      throw new BadRequestException();
    }

    return this.productRepo.findOne(productId);
  }

  async deleteProduct(productId: number, authUser: User) {
    const criteria = ProductsService.getCriteria(productId, authUser);

    const deleteResult = await this.productRepo.delete(criteria);
    if (!deleteResult.affected) {
      // need to create a proper ownership-checker
      throw new BadRequestException();
    }
  }

  private static getCriteria(productId: number, authUser: User) {
    return {
      id: productId,
      supplier: {
        id: authUser.id
      }
    };
  }
}
