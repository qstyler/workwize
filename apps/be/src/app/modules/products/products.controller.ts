import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from '@nestjs/common';
import { ProductsService } from './products.service';
import { Product, UpdateProductDto, User } from '@workwize/domain';
import { AuthUser } from '../auth/auth.user';
import { CreateProductDto } from '@workwize/domain';
import { JwtAuthGuard } from '../auth/jwt-guard';
import { OnlySupplierGuard } from '../../only.supplier/only.supplier.guard';

@Controller('products')
export class ProductsController {
  constructor(
    private productsService: ProductsService,
  ) {
  }

  @Get('')
  @UseGuards(OnlySupplierGuard)
  @UseGuards(JwtAuthGuard)
  async getProducts(
    @AuthUser() authUser: User,
  ): Promise<Product[]> {
    return this.productsService.findProducts(authUser);
  }

  @Post('')
  @UseGuards(OnlySupplierGuard)
  @UseGuards(JwtAuthGuard)
  async createProduct(
    @Body() product: CreateProductDto,
    @AuthUser() authUser: User,
  ) {
    return this.productsService.createProduct(product, authUser);
  }

  @Put(':productId')
  @UseGuards(OnlySupplierGuard)
  @UseGuards(JwtAuthGuard)
  async updateProduct(
    @Param('productId') productId: number,
    @Body() product: UpdateProductDto,
    @AuthUser() authUser: User,
  ) {
    return this.productsService.updateProduct(productId, product, authUser);
  }

  @Delete(':productId')
  @UseGuards(OnlySupplierGuard)
  @UseGuards(JwtAuthGuard)
  async deleteProduct(
    @Param('productId') productId: number,
    @AuthUser() authUser: User,
  ) {
    return this.productsService.deleteProduct(productId, authUser);
  }


}
