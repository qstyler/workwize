import { Module } from '@nestjs/common';
import { SearchController } from './search.controller';
import { SearchService } from './search.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductSchema } from '../../schemas';

@Module({
  imports: [TypeOrmModule.forFeature([ProductSchema])],
  controllers: [SearchController],
  providers: [
    SearchService
  ]
})
export class SearchModule {
}
