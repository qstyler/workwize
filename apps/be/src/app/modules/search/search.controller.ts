import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { SearchService } from './search.service';
import { JwtAuthGuard } from '../auth/jwt-guard';

@Controller('search')
export class SearchController {

  constructor(
    private searchService: SearchService,
  ) {
  }

  @UseGuards(JwtAuthGuard)
  @Get('')
  async search(
    @Query('supplierId') supplierId: number,
    @Query('q') q: string,
  ) {
    return this.searchService.search(q, supplierId);
  }
}
