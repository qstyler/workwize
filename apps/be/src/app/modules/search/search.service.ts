import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository } from 'typeorm';
import { ProductSchema } from '../../schemas';

@Injectable()
export class SearchService {

  constructor(
    @InjectRepository(ProductSchema) private productRepo: Repository<ProductSchema>
  ) {
  }

  search(q: string, supplierId: number) {
    return this.productRepo.find({
      where: {
        ...(supplierId ? { supplier: { id: supplierId } } : {}),
        ...(q ? { name: Like(`%${q}%`) } : {})
      },
      relations: ['supplier']
    });
  }
}
