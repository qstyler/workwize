import { Module } from '@nestjs/common';
import { MyOrdersController } from './my.orders.controller';
import { MyOrdersService } from './my.orders.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderSchema } from '../../schemas';

@Module({
  imports: [TypeOrmModule.forFeature([OrderSchema])],
  controllers: [MyOrdersController],
  providers: [
    MyOrdersService
  ]
})
export class MyOrdersModule {
}
