import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '@workwize/domain';

import { OrderSchema } from '../../schemas';

@Injectable()
export class MyOrdersService {
  constructor(
    @InjectRepository(OrderSchema) private orderRepo: Repository<OrderSchema>,
  ) {
  }

  async getOrders(authUser: User) {
    return await this.orderRepo.find({
      where: (qb) => {
        qb.where(`OrderSchema.completed = :completed`, {
          completed: true,
        });
        qb.andWhere(`OrderSchema__orderProducts__product__supplier.id = :supplierId`, {
          supplierId: authUser.id,
        });
      },
      relations: ['orderProducts', 'orderProducts.product', 'orderProducts.order', 'orderProducts.product.supplier', 'user'],
    });
  }
}
