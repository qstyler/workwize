import { Controller, Get, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-guard';
import { AuthUser } from '../auth/auth.user';
import { MyOrdersService } from './my.orders.service';
import { OnlySupplierGuard } from '../../only.supplier/only.supplier.guard';
import { User } from '@workwize/domain';

@Controller('myorders')
export class MyOrdersController {

  constructor(
    private myOrdersService: MyOrdersService,
  ) {
  }

  @UseGuards(OnlySupplierGuard)
  @UseGuards(JwtAuthGuard)
  @Get('')
  async getOrders(
    @AuthUser() authUser: User,
  ) {
    return this.myOrdersService.getOrders(authUser);
  }
}
