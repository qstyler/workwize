import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt'
import { UserService } from '../user/user.service';
import { UserLoginPayload, User } from '@workwize/domain';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
  ) {}

  async createToken(user: User) {
    const {password, ...jwtPayload} = user;
    return {
      expiresIn: 600,
      accessToken: this.jwtService.sign(jwtPayload),
      jwtPayload,
    };
  }

  async validateUser(payload: UserLoginPayload): Promise<any> {
    const user = await this.userService.findByEmailAndPassword(payload.email, payload.password);
    if (!user) {
      throw new UnauthorizedException('Invalid credentials!');
    }
    return user;
  }
}
