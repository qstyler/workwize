import { Body, Controller, Post } from '@nestjs/common';
import { UserService } from '../user/user.service';

import { AuthService } from './auth.service';
import { UserLoginPayload, UserRegisterPayload } from '@workwize/domain';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @Post('login')
  async login(@Body() payload: UserLoginPayload): Promise<any> {
    const user = await this.authService.validateUser(payload);
    return await this.authService.createToken(user);
  }

  @Post('register')
  async register(@Body() payload: UserRegisterPayload): Promise<any> {
    const user = await this.userService.create(payload);
    return await this.authService.createToken(user);
  }
}
