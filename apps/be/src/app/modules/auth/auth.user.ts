import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { User } from '@workwize/domain';

export const AuthUser = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): User => {
    const { user } = ctx.switchToHttp().getRequest();
    return user;
  },
);
