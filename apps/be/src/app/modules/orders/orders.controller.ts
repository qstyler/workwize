import { Controller, Get, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-guard';
import { AuthUser } from '../auth/auth.user';
import { OrdersService } from './orders.service';
import { User } from '@workwize/domain';

@Controller('orders')
export class OrdersController {

  constructor(
    private ordersService: OrdersService,
  ) {
  }

  @UseGuards(JwtAuthGuard)
  @Get('')
  async getOrders(
    @AuthUser() authUser: User,
  ) {
    return this.ordersService.getOrders(authUser);
  }
}
