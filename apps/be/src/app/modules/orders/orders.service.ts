import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { OrderSchema } from '../../schemas';
import { User } from '@workwize/domain';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(OrderSchema) private orderRepo: Repository<OrderSchema>,
  ) {
  }

  getOrders(authUser: User) {
    return this.orderRepo.find({
      where: {
        completed: true,
        user: { id: authUser.id },
      },
      relations: ['orderProducts', 'orderProducts.product'],
    });
  }
}
