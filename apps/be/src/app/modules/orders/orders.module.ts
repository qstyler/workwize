import { Module } from '@nestjs/common';
import { OrdersController } from './orders.controller';
import { OrdersService } from './orders.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderSchema } from '../../schemas';

@Module({
  imports: [TypeOrmModule.forFeature([OrderSchema])],
  controllers: [OrdersController],
  providers: [
    OrdersService
  ]
})
export class OrdersModule {
}
