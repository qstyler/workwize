import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserService } from './user.service';
import { UserSchema } from '../../schemas';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserSchema])
  ],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {}
