import { Injectable, NotAcceptableException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRegisterPayload, User } from '@workwize/domain';
import { Repository } from 'typeorm';
import { UserSchema } from '../../schemas';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserSchema)
      private userRepo: Repository<UserSchema>
  ) {
    // super
  }

  async findById(id: number): Promise<User> {
    return this.userRepo.findOne(id);
  }

  async findByEmail(email: string): Promise<User> {
    return this.userRepo.findOne({
      email
    });
  }

  async findByEmailAndPassword(email: string, password: string): Promise<User> {
    return this.userRepo.findOne({
      email, password
    });
  }

  async create(payload: UserRegisterPayload) {
    const user = await this.findByEmail(payload.email);
    if (user) {
      throw new NotAcceptableException('User with provided email already created');
    }
    return await this.userRepo.save(payload);
  }
}
