import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthModule } from './modules/auth/auth.module';
import { ProductsModule } from './modules/products/products.module';
import { SearchModule } from './modules/search/search.module';
import { CartModule } from './modules/cart/cart.module';
import { OrdersModule } from './modules/orders/orders.module';
import { MyOrdersModule } from './modules/my.orders/my.orders.module';
import { ConfigModule } from '@nestjs/config';

import * as Schemas from './schemas';

import { environment } from '../environments/environment';

@Module({
  imports: [
    AuthModule,
    ProductsModule,
    SearchModule,
    CartModule,
    OrdersModule,
    MyOrdersModule,
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      useFactory: async () => ({
        ...(await environment()).database,
        entities: Object.values(Schemas),
      })
    }),
  ],
  controllers: [AppController],
  providers: [
    AppService,
  ],
})
export class AppModule {
}
