import { ProductSchema } from './product';
import { OrderSchema } from './order';
import { OrderProduct } from '../../../../../libs/domain/src';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity('order_products')
export class OrderProductSchema implements OrderProduct {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => ProductSchema)
  product: ProductSchema;

  @ManyToOne(() => OrderSchema)
  order: OrderSchema;

  @Column()
  quantity: number;
}
