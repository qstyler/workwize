import { User } from '../../../../../libs/domain/src/interfaces';
import { Column, Entity, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users')
export class UserSchema implements User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column({
    select: false,
  })
  password: string;

  @Column({
    default: false,
  })
  isSupplier: boolean;
}
