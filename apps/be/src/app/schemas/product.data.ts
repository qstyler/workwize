import { ProductData } from '../../../../../libs/domain/src/interfaces';
import { Column } from 'typeorm';

export class ProductDataColumn implements ProductData {

  @Column()
  name: string;

  @Column()
  price: number;
}
