import { UserSchema } from './user';
import { ProductDataColumn } from './product.data';
import { Product } from '../../../../../libs/domain/src/interfaces';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity('products')
export class ProductSchema implements Product {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => UserSchema)
  supplier: UserSchema;

  @Column(() => ProductDataColumn, { prefix: false })
  productData: ProductDataColumn;
}
