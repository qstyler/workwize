import { UserSchema } from './user';
import { OrderProductSchema } from './order.product';
import { Order } from '../../../../../libs/domain/src/interfaces';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('orders')
export class OrderSchema implements Order {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => UserSchema)
  user: UserSchema;

  @Column()
  completed: boolean;

  @Column({
    nullable: true,
  })
  date?: Date;

  @OneToMany(() => OrderProductSchema, (orderProduct) => orderProduct.order)
  orderProducts: OrderProductSchema[];
}
