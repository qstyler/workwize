# [Workwize](https://www.goworkwize.com/) test assignment

A fully operational web catalogue enabling multiple suppliers to sell items to multiple users.

## Tech data
### Database 

![database structure](./assets/database.png)

For the simplicity of the project, the database consists of only 4 models:
* **products** — represent a product with its basic info: price and name. This record also has a pointer to users table called **supplier** 
* **users** — represent any person able to login to the website. **isSupplier** flag gives a user supplier privileges: like creating products and corresponding orders overview.
* **order_products** — a table connecting products with orders, it also specifies a quantity of products in the order.
* **orders** — is used for both orders and cart. Because a cart is basically an order that is not complete yet. And for that we have a boolean flag **completed**. An order is created as a cart and is incomplete.

### Structure

The project is build with [nx](https://nx.dev) monorepo management tool and consists of 3 parts:
* **domain** — a library that has all domain description: interfaces and payloads. Payloads are annotated with [class-validator](https://www.npmjs.com/package/class-validator) a very powerful validation tool. Using the same validation rules in both front and backend of application helps a lot with consistency of the product.
* **be** — project's api. Built with [NestJS](https://nestjs.com/) a powerful node.js framework with angular-style modular structure that boosts the development. Backend project is using [TypeORM](https://typeorm.io/#/) library for database structure implementation in javascript.
* **fe** — frontend. A [React](https://reactjs.org/) application with use of [hooks](https://reactjs.org/docs/hooks-intro.html) and [react hook form](https://react-hook-form.com/) for state management and [bootstrap](https://getbootstrap.com/) for styling.

### Databases
* **SQLite** is used as a persistent storage in a local environment because it was specified in the task.
* **MySQL** is used in the production environment. Because you don't really use SQLite in the cloud.

### Deployment
The project is using [Google Cloud's](https://cloud.google.com/) [App Engine](https://cloud.google.com/appengine) because in my experience it's the shortest way to have your application deployed. It requires no containerization or extra configuration. Can hook up with [custom domain](https://workwize.info) in just few clicks. And gives $ 300 credit fora newcomer user. Which means you can try it for free every time you register new google (gmail) account.

### Source Hosting
[GitLab](https://gitlab.com/) provides very powerful source hosting functionality along with free tier of [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) that is also widely used for project's delivery. 

## Functionality

#### Shortcuts
* **Only registered users**. All functionality is available for authenticated users only.
* **Common dashboard**. Suppliers and regular visitors use the same interface. With few additional routes available for suppliers. It was just easy to put all the navigation and routing in one Component with a _isLogged_ and _isSupplier_ flags assertion. It wouldn't be a problem to separate this representation in two different dashboards. Although I think that suppliers should be allowed to shop other suppliers' goods.
* **Products CRUD on one screen**. It would be too much work to split all these into different pages and create all this routing etc. One screen is ok.
* **Cart's count and total** is calculated at the frontend. Since we don't store this information anywhere on the backend I thought it wouldn't be a huge difference where to calculate it.

### Supplier environment

![different environments](./assets/differentenvironments.gif)

#### Able to register and login
This functionality is fully operational.
![register](./assets/register.gif)

#### Able toCRUD products
As I mentioned, everything is one screen, but yet works absolutely fine.
![crud](./assets/crud.gif)

#### View which users bought your products
This request will run a query on a backend that will return all orders with products that belong to current supplier user.

#### Anything you deem as an appropriate addition to the app
When querying suppliers' orders the products and amount you get is only based on current suppliers' products.

### Shopping environment

#### Able to register and login
Well we've seen it working already

#### Able to view all products created by suppliers
Search functionality just works.

#### Able to add to cart, checkout, and purchase products
You can put products to a cart and checkout at the checkout page
![checkout](./assets/checkout.gif)

#### Able to view order history
The order history is available in “Customer Orders” page

#### Anything you deem as an appropriate addition to the app
You can filter products by supplier by clicking on supplier's email in the list
![filter](./assets/filter.gif)

## Deliverable

### Local installation:

It is aaaaabsolutely easy.

Clone the repo. Install the dependencies and just run it. It should be enough.

```shell
git@gitlab.com:qstyler/workwize.git
```

```shell
npm install
```

```shell
npm run serve:all
```

Open local url have fun: http://localhost:4200/
